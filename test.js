const {
    knexQuery
} = require('./index.js')({
    type: 'mysql',
    connection: {
        "host": "127.0.0.1",
        "user": "root",
        "password": "u",
        "database": "cinder",
        "port": 3306
    },
})
const runTest = async () => {
    const response = await knexQuery({
        action: "select",
        columns: "*",
        table: "users",
    })
    console.log(response)
}
runTest()
