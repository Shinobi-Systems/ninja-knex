module.exports = function(options){
    if(!options.type)options.type = 'mysql'
    const databaseOptions = {
        client: options.type,
        connection: options.connection,
    }
    const databaseEngine = require('knex')(databaseOptions)
    const debugLog = (...args) => {
        console.log(...args)
    }
    const cleanSqlWhereObject = (where) => {
        const newWhere = {}
        Object.keys(where).forEach((key) => {
            if(key !== '__separator'){
                const value = where[key]
                newWhere[key] = value
            }
        })
        return newWhere
    }
    const processSimpleWhereCondition = (dbQuery,where,didOne) => {
        var whereIsArray = where instanceof Array;
        if(where[0] === 'or' || where.__separator === 'or'){
            if(whereIsArray){
                where.shift()
                dbQuery.orWhere(...where)
            }else{
                where = cleanSqlWhereObject(where)
                dbQuery.orWhere(where)
            }
        }else if(!didOne){
            didOne = true
            whereIsArray ? dbQuery.where(...where) : dbQuery.where(where)
        }else{
            whereIsArray ? dbQuery.andWhere(...where) : dbQuery.andWhere(where)
        }
    }
    const processWhereCondition = (dbQuery,where,didOne) => {
        var whereIsArray = where instanceof Array;
        if(!where[0])return;
        if(where[0] && where[0] instanceof Array){
            dbQuery.where(function() {
                var _this = this
                var didOneInsideGroup = false
                where.forEach((whereInsideGroup) => {
                    processWhereCondition(_this,whereInsideGroup,didOneInsideGroup)
                })
            })
        }else if(where[0] && where[0] instanceof Object){
            dbQuery.where(function() {
                var _this = this
                var didOneInsideGroup = false
                where.forEach((whereInsideGroup) => {
                    processSimpleWhereCondition(_this,whereInsideGroup,didOneInsideGroup)
                })
            })
        }else{
            processSimpleWhereCondition(dbQuery,where,didOne)
        }
    }
    const knexQuery = (options) => {
        const response = {
            ok: true,
            rows: []
        }
        return new Promise((resolve,reject) => {
            try{
                // options = {
                //     action: "",
                //     columns: "",
                //     table: ""
                // }
                var dbQuery
                switch(options.action){
                    case'select':
                        options.columns = options.columns.indexOf(',') === -1 ? [options.columns] : options.columns.split(',');
                        dbQuery = databaseEngine.select(...options.columns).from(options.table)
                    break;
                    case'count':
                        options.columns = options.columns.indexOf(',') === -1 ? [options.columns] : options.columns.split(',');
                        dbQuery = databaseEngine(options.table)
                        dbQuery.count(options.columns)
                    break;
                    case'update':
                        dbQuery = databaseEngine(options.table).update(options.update)
                    break;
                    case'delete':
                        dbQuery = databaseEngine(options.table)
                    break;
                    case'insert':
                        dbQuery = databaseEngine(options.table).insert(options.insert)
                    break;
                }
                if(options.where instanceof Array){
                    var didOne = false;
                    options.where.forEach((where) => {
                        processWhereCondition(dbQuery,where,didOne)
                    })
                }else if(options.where instanceof Object){
                    dbQuery.where(options.where)
                }
                if(options.action === 'delete'){
                    dbQuery.del()
                }
                if(options.orderBy){
                    dbQuery.orderBy(...options.orderBy)
                }
                if(options.groupBy){
                    dbQuery.groupBy(options.groupBy)
                }
                if(options.limit){
                    if(`${options.limit}`.indexOf(',') === -1){
                        dbQuery.limit(options.limit)
                    }else{
                        const limitParts = `${options.limit}`.split(',')
                        dbQuery.limit(limitParts[1]).offset(limitParts[0])
                    }
                }
                if(options.debugLog === true){
                    console.log(dbQuery.toString())
                }
                dbQuery.asCallback(function(err,r) {
                    if(err){
                        response.error = err
                        if(options.debugLogVerbose && options.debugLog === true){
                            response.stacktrace = new Error()
                        }
                    }
                    response.rows = r
                    resolve(response)
                })
            }catch(err){
                response.error = err
                resolve(response)
            }
        })
    }
    return {
        knexQuery: knexQuery,
        cleanSqlWhereObject: cleanSqlWhereObject,
        processSimpleWhereCondition: processSimpleWhereCondition,
        processWhereCondition: processWhereCondition,
        databaseOptions: databaseOptions,
        databaseEngine: databaseEngine,
    }
}
